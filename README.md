# Taking FEEC Seriously and Literally

Finite Element Exterior Calculus (FEEC) is a powerful tool for analyzing all of
the finite element spaces in the de Rham complex --- H^1, H(curl), H(div), L^2
--- as one.  This unity is part of the public interface of popular finite
element toolkits, but not as often part of the implementation.

This talk will present my experiences so far in using FEEC and other concepts,
like compatible finite element systems and abstract polytopes, to define
a highly configurable finite element interface in the discretization tools of
PETSc, the Portable Extensible Toolkit for Scientific Computing.  Those
experiences include issues of software design (how to support non-conforming
meshes in lattice representations of meshes) and performance (how to combine
more exotic spaces with high-performance element kernel interfaces like
libCEED).

Taking FEEC literally when writing finite elements code has also led to a new
contribution to FEEC theory.  I will show how a single method for building
trace-free functions, combined with a single extension operator, can construct
all of the major FEEC elements on the simplex.

